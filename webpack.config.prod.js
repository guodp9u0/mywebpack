var webpack = require("webpack");
var HtmlWebpackPlugin = require("html-webpack-plugin");
var config = require("./webpack.config");

//customer config
config.devtool = "source-map";
if (!config.plugins) {
    config.plugins = [];
}
//config.plugins.push(new webpack.optimize.UglifyJsPlugin({
//    compressor: {
//        warnings: false
///    }
//}))
module.exports = config;