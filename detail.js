//import Please from 'pleasejs'

if (module.hot) {
    module.hot.accept()
}
var Please = require('pleasejs');
require('./src/css/home.css');
require('./src/css/common.css');
var div = document.getElementById('color');
var button = document.getElementById('button');

const changeColor = () => {
    if (div) {
        div.style.backgroundColor = Please.make_color();
    }
}
if (button) {
    button.addEventListener('click', changeColor)
}