var webpack = require("webpack");
var HtmlWebpackPlugin = require("html-webpack-plugin");
var config = require("./webpack.config");

//customer config
config.devtool = 'cheap-eval-source-map';
if (!config.plugins) {
    config.plugins = [];
}
config.plugins.push(new webpack.HotModuleReplacementPlugin());

config.devServer = {
    contentBase: './dist',
    hot: true
}
module.exports = config;