var path = require("path");
var webpack = require("webpack");
var HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    devtool: 'cheap-eval-source-map',
    entry: {
        index: [
            './index'
        ],
        detail: [
            './detail'
        ],
        vendors: [
            'jquery',
            'fastclick',
            'pleasejs',
            `${__dirname}/src/js/lib/spin.js`
        ]
    },
    output: {
        filename: '[name].[hash].js',
        path: `${__dirname}/dist`,
        //filename: "bundle.js",
        //chunkFilename: 'chunk/chunk[id][name]-[chunkhash:6].js',
    },
    postcss: function() {
        return {
            defaults: [autoprefixer],
            cleaner: [autoprefixer({ browsers: ["ios >= 7", "android >= 4.0"] })]
        };
    },
    resolve: {
        alias: { //它的作用是把用户的一个请求重定向到另一个路径
            //'redux-devtools/lib': path.join(__dirname, '..', '..', 'src'),//这些但是demo自定义的
            //'redux-devtools': path.join(__dirname, '..', '..', 'src'),
            //'react': path.join(__dirname, 'node_modules', 'react'),
            //'moment': "moment/min/moment-with-locales.min.js"
        },
        extensions: ['', '.js', '.scss', '.css', '.png', 'jpg', 'jpeg']
    },
    resolveLoader: {
        'fallback': path.join(__dirname, 'node_modules') //定义绝对路径
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendors', // 将公共模块提取，生成名为`vendors`的chunk
            filename: 'vendors.[hash].js',
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'commom', // 将公共模块提取，生成名为`vendors`的chunk
            chunks: ['index', 'detail'], //提取哪些模块共有的部分
            minChunks: 3
        }), new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),
        new webpack.NoErrorsPlugin(),
        new HtmlWebpackPlugin({
            template: './index.html', //html模板路径
            favicon: './src/img/favicon.ico', //favicon路径，通过webpack引入同时可以生成hash值
            filename: './index.html', //生成的html存放路径，相对于path
            inject: true, //js插入的位置，true/'head'/'body'/false
            hash: true, //为静态资源生成hash值
            chunks: ['vendors', 'index'], //需要引入的chunk，不配置就会引入所有页面的资源
            minify: { //压缩HTML文件
                removeComments: true, //移除HTML中的注释
                collapseWhitespace: false //删除空白符与换行符
            }
        }),
        new HtmlWebpackPlugin({ //根据模板插入css/js等生成最终HTML
            favicon: './src/img/favicon.ico', //favicon路径，通过webpack引入同时可以生成hash值
            filename: './detail.html', //生成的html存放路径，相对于path
            template: './detail.html', //html模板路径
            inject: true, //js插入的位置，true/'head'/'body'/false
            hash: true, //为静态资源生成hash值
            chunks: ['vendors', 'detail'], //需要引入的chunk，不配置就会引入所有页面的资源
            minify: { //压缩HTML文件
                removeComments: true, //移除HTML中的注释
                collapseWhitespace: false //删除空白符与换行符
            }
        })
    ],
    module: {
        loaders: [{
                test: /\.css$/,
                loaders: ['style-loader', 'css-loader']
            }, {
                test: /\.js$/,
                loaders: ['babel-loader'],
                include: path.join(__dirname, 'src')
            }, {
                test: /\.(jpe?g|png|gif|svg)$/,
                loader: 'url-loader',
                query: {
                    limit: 100,
                    name: 'resource/img/[name].[hash].[ext]'
                },
                exclude: /node_modules/,
                include: path.resolve(__dirname, 'src')
            },
            {
                test: /\.html$/,
                loaders: ['html-loader'],
            }
        ]
    },
    babel: {
        presets: ['es2015', 'stage-0'],
        plugins: ['transform-runtime']
    }
}